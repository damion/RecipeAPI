﻿using Interfaces;

namespace Repositories
{
    public static class RepoFactory
    {
        private static IIngredientRepository ingredientRepo;
        public static IIngredientRepository IngredientRepo {
            get
            {
                return ingredientRepo ?? new IngredientRepository();
            }
            set
            {
                ingredientRepo = value;
            }
        }

    }
}
