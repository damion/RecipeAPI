﻿using RecipeData.Repositories;
using System.Collections.Generic;

namespace Mappers
{
    public abstract class AbstractMapper<T, I, R>
        where T : class, new()
        where I : ApiRecipeItem
        where R : AbstractRepository<T>
    {
        public abstract IEnumerable<I> GetAll();
        public abstract I GetById(int id);
        public abstract void Save(int id, I itemToSave);
        public abstract void Save(I itemToSave);
        public abstract void Delete(int id);

        public abstract T DataObjectFromApiObject(I sourceItem);
    }
}
