﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mappers
{
    public abstract class ApiRecipeItem { }

    public class ApiIngredient : ApiRecipeItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}