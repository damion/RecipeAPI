﻿using RecipeData.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeData.Classes
{
    public class RecipeIngredient
    {
        public RecipeIngredientGroup Group { get; set; }

        public int Id { get; set; }

        public Ingredient Ingredient { get; set; }

        public string Quantity { get; set; }

        public MeasurementUnit Unit { get; set; }

        public Recipe Recipe { get; set; }

    }
}
