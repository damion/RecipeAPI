namespace RecipeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialschemamigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RecipeIngredientGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecipeId = c.Int(nullable: false),
                        GroupDescriptor = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RecipeIngredients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        GroupId = c.Int(nullable: false),
                        Name = c.String(),
                        Quantity = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Recipes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Categories = c.Int(nullable: false),
                        Description = c.String(),
                        Name = c.String(),
                        Notes = c.String(),
                        PrepTime = c.String(),
                        Yield = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Recipes");
            DropTable("dbo.RecipeIngredients");
            DropTable("dbo.RecipeIngredientGroups");
        }
    }
}
