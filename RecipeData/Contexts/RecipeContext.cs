﻿using RecipeData.Classes;
using System.Data.Entity;

namespace RecipeData.Contexts
{
    public class RecipeContext : DbContext
    {
        public DbSet<RecipeIngredient> RecipeIngredients { get; set; }
        public DbSet<RecipeIngredientGroup> RecipeIngredientGroups { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<MeasurementUnit> MeasurementUnits { get; set; }
    }
}
